// Copyright 2023 University of Washington
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "code_timing/code_timing.h"

#include "code_timing/CodeBlockTiming.h"

namespace code_timing {

CodeTiming::CodeTiming(ros::NodeHandle &nh, const std::string &node_name)
    : _pub(nh.advertise<CodeBlockTiming>("/timing", 1)), _node_name() {
  if (node_name.empty()) {
    _node_name = ros::this_node::getName();
  } else {
    _node_name = node_name;
  }
}

CodeTimingBlock CodeTiming::startBlock(const std::string &metric,
                                       const std::string &system,
                                       const std::string &subsystem) {
  return CodeTimingBlock(*this, metric, system, subsystem);
}

void CodeTiming::publish(CodeBlockTimingPtr msg) {
  msg->nodename = _node_name;
  _pub.publish(msg);
}

//~~~~~ CodeTimingBlock ~~~~~

CodeTimingBlock::CodeTimingBlock(CodeTiming &parent, const std::string &metric,
                                 const std::string &system,
                                 const std::string &subsystem)
    : _parent(parent),
      _metric(metric),
      _system(system),
      _subsystem(subsystem),
      _start(std::chrono::high_resolution_clock::now()) {}

CodeTimingBlock::~CodeTimingBlock() {
  double time_taken = std::chrono::duration_cast<std::chrono::microseconds>(
                          std::chrono::high_resolution_clock::now() - _start)
                          .count();

  code_timing::CodeBlockTimingPtr msg(new code_timing::CodeBlockTiming);

  msg->header.stamp = ros::Time::now();
  msg->metric = _metric;
  msg->system = _system;
  msg->subsystem = _subsystem;
  msg->microseconds = time_taken;
  _parent.publish(msg);
}

}  // namespace code_timing
